import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Alex on 29-Apr-16.
 */
public class Group {

    private List<Student> studentList;
    private int groupId;
    private String curatorLastName;
    private String speciality;
    private Curator curator;
    private RandomNumbersGenerator groupNumberGenerator = new RandomNumbersGenerator();

    public Group() {
    }

    public Group(int groupId, String speciality, Curator curator) {
        this.groupId = groupId;
        this.speciality = speciality;
        this.curator = curator;
        studentList = new ArrayList<>();
    }

    public Group(String speciality, Curator curator) {
        this.speciality = speciality;
        this.curator = curator;
        setGroupId(groupId);
        studentList = new ArrayList<>();
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupNumberGenerator.setGroupId();
    }

    public String getCuratorLastName() {
        return curatorLastName =  curator.getLastName();
    }

    public void setCuratorLastName(String curatorLastName) {
        this.curatorLastName = curatorLastName;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public int size() {
        return studentList.size();
    }

    public void addStudentToGroup(Student s1) {
        s1.setGroupId(getGroupId());
        studentList.add(s1);
    }

    public void printGroup() {
        System.out.println(studentList.toString().replace("[", "").replace("]", ""));
    }

    public void sortGroupByFullName() {
        System.out.println("\n" + "SORTING BY FULL NAME");
        studentList.sort(Comparator.comparing(Student::getFullName));
    }

    public void sortGroupByAge() {
        System.out.println("\n" + "SORTING BY AGE");
        studentList.sort(Comparator.comparing(Student::getAge));
    }

    public void sortGroupByStudentId() {
        System.out.println("\n" + "SORTING BY STUDENT ID");
        studentList.sort(Comparator.comparing(Student::getStudentId));
    }

    public void sortGroupByDateOfBirth() {
        System.out.println("\n" + "SORTING BY DATE OF BIRTH");
        studentList.sort(Comparator.comparing(Student::getDateOfBirth));
    }

    public void writeToFile(String filename) {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "utf-8"))) {
            writer.write(studentList.toString().replace("[", "").replace("]", ""));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "\n" + "Group " + groupId +
                ", curatorLastName='" + curatorLastName + '\'' +
                ", speciality='" + speciality + '\'' +
                '}';
    }

//    public void readFromFileExtra(String filename) throws IOException {
//        List<String> list = Files.readAllLines(new File(filename).toPath(), Charset.defaultCharset());
//        System.out.println(list.toString());
//    }
}


//student list (set or list)
//alphabetic order

//list students
//addtoset (compare equals)
//writetofile (string filename)
//readfromfile (string filename) - повертає групу


//group gr = new group
//при додаванні міняємо студента, додаємо групу.

//generator id (list integer) - new class
//list integer ids
//int getID() - додає новий айді
//math.random