//package main;
//
//import java.sql.*;
//
///**
// * Created by Alex on 04-May-16.
// */
//public class ParamTest1 {
//    //DriverManager - getconnection
//    //Connection - createstatement
//    //Statement - execute() - return resultset, executeupdate() - return int
//    //ResultSet
//    //PreparedStatement - sql "Select * from User u Where u.id=:id"
//            //st.setInteger("id", 5)
//    //setConnection (jdbc:mysql://localhost:3306/mydb
//
//    public static void main(String[] args) throws Exception {
//        Connection connection = null;
//        Statement statement = null;
//        ResultSet resultSet = null;
//
//        try {
//            Class.forName("com.mysql.jdbc.Driver");
//            String url = "jdbc:mysql://localhost:3306/students";
//            connection = DriverManager.getConnection(url, "admin", "admin");
//            statement = connection.createStatement();
//            resultSet = statement.executeQuery("SELECT * FROM students");
//            while (resultSet.next()) {
//                String str = resultSet.getString(1) + ":" + resultSet.getString(2);
//                System.out.println(str);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (resultSet != null) {
//                    resultSet.close();
//                } if (statement != null) {
//                    statement.close();
//                } if (connection != null) {
//                    connection.close();
//                }
//            } catch (SQLException ex) {
//                ex.printStackTrace();
//                System.err.println("Error!");
//            }
//        }
//
//        System.out.println("hello");
//    }
//
////    public void main() {
////        String user = "aa";
////        String pass = "bb";
////        String url = "jdbc:mysql....";
////        String driver = "org.mysql....";
////
////        try {
////            Class.forName(driver);
////        } catch (ClassNotFoundException e) {
////            e.printStackTrace();
////        }
////
////        try {
////
////            Connection c = new DriverManager.getConnection(url, user, pass);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//}
