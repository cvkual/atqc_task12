import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Alex on 29-Apr-16.
 */
public class Student {
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private int age;
    private int studentId; // random number generator
    private Gender gender;
    private int groupId;
    private RandomNumbersGenerator studentIdNumberGenerator = new RandomNumbersGenerator();

    public Student() {

    }

    public Student(ResultSet resultSet) throws SQLException {
        setStudentId(resultSet.getInt(1));
        setFirstName(resultSet.getString(2));
        setLastName(resultSet.getString(3));
        setDateOfBirth(resultSet.getDate(4));
        setGender(Gender.valueOf(resultSet.getString(5)));
        setGroupId(resultSet.getInt(6));
    }

    public Student(String firstName, String lastName, int dob_y, int dob_m, int dob_d, Gender gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        setDateOfBirth(dob_y, dob_m, dob_d);
        this.gender = gender;
        setStudentId(studentId);
    }

    public Student(int groupId, String firstName, String lastName, int dob_y, int dob_m, int dob_d, Gender gender) {
        this.groupId = groupId;
        this.firstName = firstName;
        this.lastName = lastName;
        setDateOfBirth(dob_y, dob_m, dob_d);
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(int dob_y, int dob_m, int dob_d) {
        this.dateOfBirth = new LocalDate(dob_y, dob_m, dob_d);
    }

    public void setDateOfBirth(Date date) {
        this.dateOfBirth = LocalDate.fromDateFields(date);
    }

    public int getAge() {
        Period period = new Period(dateOfBirth, new LocalDate(), PeriodType.yearMonthDay());
        age = period.getYears();
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentIdNumberGenerator.setStudentId();
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int group) {
        this.groupId = group;
    }

    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }


    @Override
    public String toString() {
        return  "\n" + "Student{" +
                "groupId=" + groupId +
                ", studentId=" + studentId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", gender=" + gender +
                '}';
    }

    public boolean equals(Object other) {
        if (other == this) return true;
        if (other == null) return false;
        if (other.getClass() != this.getClass()) return false;
        Student that = (Student) other;
        return (this.studentId == that.studentId) && (this.getFullName().equals(that.getFullName()));
    }
}
//
//    public static int StudentCompareByFullName(Student s1, Student s2) {
//        if (s1.getFullName().equals(s2.getFullName())) {
//            return s1.getAge() - s2.getAge();
//        } else {
//            return s1.getFullName().compareTo(s2.getFullName());
//        }
//    }
//
//    Comparator<Student> comp = new Comparator<Student>() {
//        @Override
//        public int compare(Student o1, Student o2) {
//            return o1.getFullName().compareTo(o2.getFullName());
//        }
//    };
//}
//
//class StudentComparatorByFullName implements Comparator<Student> {
//
//    @Override
//    public int compare(Student o1, Student o2) {
//        return o1.getFullName().compareTo(o2.getFullName());
//    }
//}
//
//class StudentComparatorByDateOfBirth implements Comparator<Student> {
//        @Override
//        public int compare(Student o1, Student o2) {
//            return o1.getDateOfBirth().compareTo(o2.getDateOfBirth());
//        }
//}

//getage
//sex - enum
//studentId 6 numbers
//group

//to string
//equals

//compare fullname(