import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Alex on 04-May-16.
 */
public class JDBC {
    private static Connection connection;

    public JDBC() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/students";
            connection = DriverManager.getConnection(url, "root", "root");
        } catch (ClassNotFoundException | SQLException e) {
            throw new Exception(e);
        }
    }

    public void createTableStudents() {
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            String sql = "CREATE TABLE students " +
                    "(student_id INTEGER not NULL, " +
                    " firstName VARCHAR(30), " +
                    " lastName VARCHAR(30), " +
                    " dateOfBirth DATE, " +
                    " gender VARCHAR(10)," +
                    " group_id INTEGER NOT NULL," +
                    " PRIMARY KEY ( id ))";
            stmt.execute(sql);
            System.out.println("Table created");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void createTableGroups() {
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            String sql = "CREATE TABLE groups " +
                    "(group_id INTEGER NOT NULL," +
                    "curator VARCHAR(30)," +
                    "speciality VARCHAR(30)," +
                    "PRIMARY KEY (group_id))";
            stmt.execute(sql);
            System.out.println("Table created");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int getIdByFullName(String firstName, String lastName) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        int id = 0;
        try {
            statement = connection.prepareStatement("SELECT student_id FROM students WHERE firstName=? AND lastName=?");
//            System.out.println("Please enter first name: ");
//            Scanner scanner = new Scanner(System.in);
//            String firstName = scanner.nextLine();
//            System.out.println("Please enter last name: ");
//            String lastName = scanner.nextLine();
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                id = resultSet.getInt(1);
//                System.out.println(id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return id;
    }

    public void insertStudents(Student student) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("INSERT INTO students" + "(student_id, firstName, lastName, dateOfBirth, gender, group_id)" + "VALUES (?, ?, ?, ?, ?, ?)");
            statement.setInt(1, student.getStudentId());
            statement.setString(2, student.getFirstName());
            statement.setString(3, student.getLastName());
            statement.setDate(4, java.sql.Date.valueOf(String.valueOf(student.getDateOfBirth())));
            statement.setString(5, String.valueOf(student.getGender()));
            statement.setInt(6, student.getGroupId());
            statement.execute();
            System.out.println("Student added successfully");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void insertGroups(Group group) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("INSERT INTO groups" + "(group_id, curator, speciality)" + "VALUES (?, ?, ?)");
            statement.setInt(1, group.getGroupId());
            statement.setString(2, group.getCuratorLastName());
            statement.setString(3, group.getSpeciality());
            statement.execute();
            System.out.println("Group added successfully");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ArrayList<Student> getAllStudents() {
         ArrayList<Student> students = new ArrayList<Student>();

        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(
                    "SELECT student_id, firstName, lastName, dateOfBirth, gender, group_id FROM students ORDER BY group_id");
            while (resultSet.next()) {
                Student st = new Student(resultSet);
                students.add(st);
            }
//            System.out.println(students);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return students;
    }

    public ArrayList<Student> getStudentsFromGroup(int groupId) {
        ArrayList<Student> students = new ArrayList<Student>();

        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(
                    "SELECT student_id, firstName, lastName, dateOfBirth, gender, group_id FROM students WHERE group_id=? ORDER BY lastName, firstName");
            statement.setInt(1, groupId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Student st = new Student(resultSet);
//                students.add(new Student());
                students.add(st);
            }
//            System.out.println(students);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return students;
    }

    public void moveStudentsToGroup(int oldGroupId, int newGroupId) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(
                    "UPDATE students SET group_id=?" + "WHERE group_id=?");
            statement.setInt(1, oldGroupId);
            statement.setInt(2, newGroupId);
            statement.execute();
            System.out.println("Students moved successfully");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<Group> getGroups() {
        List<Group> groups = new ArrayList<Group>();

        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT group_id, curator, speciality FROM groups");
            while (resultSet.next()) {
                Group gr = new Group();
                gr.setGroupId(resultSet.getInt(1));
                gr.setCuratorLastName(resultSet.getString(2));
                gr.setSpeciality(resultSet.getString(3));
                groups.add(gr);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println(groups);
        return groups;
    }

    public void removeStudentsFromGroup(int groupId) {
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(
                    "DELETE FROM students WHERE group_id=?");
            stmt.setInt(1, groupId);
            stmt.execute();
            System.out.println("Students removed successfully");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void updateStudent(Student student) {
        PreparedStatement stmt = null;
        try {
            System.out.println("Please select group for update: ");
            Scanner keyboard = new Scanner(System.in);
            int number = keyboard.nextInt();
            stmt = connection.prepareStatement(
                    "UPDATE students SET " +
                            "firstName=?, lastName=?, " +
                            "dateOfBirth=?, gender=?, group_id=?" +
                            "WHERE student_id=?");
            stmt.setString(1, student.getFirstName());
            stmt.setString(2, student.getLastName());
            stmt.setDate(3, java.sql.Date.valueOf(String.valueOf(student.getDateOfBirth())));
            stmt.setString(4, String.valueOf(student.getGender()));
            stmt.setInt(5, student.getGroupId());
            stmt.setInt(6, number);
            stmt.execute();
            System.out.println("Student updated successfully");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void updateGroup(Group group) {
        PreparedStatement statement = null;
        try {
            System.out.println("Please select group for update: ");
            Scanner keyboard = new Scanner(System.in);
            int number = keyboard.nextInt();
            statement = connection.prepareStatement("UPDATE groups SET " + "curator=?, speciality=? WHERE group_id=?");
            statement.setString(1, group.getCuratorLastName());
            statement.setString(2, group.getSpeciality());
            statement.setInt(3, number);
            statement.execute();
            System.out.println("Group updated successfully");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void deleteStudent(int studentId) {
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(
                    "DELETE FROM students WHERE student_id=?");
            stmt.setInt(1, studentId);
            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void removeGroup(int groupId) {
        PreparedStatement statement = null;
        PreparedStatement st = null;
        try {

            statement = connection.prepareStatement("DELETE FROM groups WHERE group_id=?");
            statement.setInt(1, groupId);

            String queryCheck = "SELECT group_id FROM students WHERE students.group_id=?";
            st = connection.prepareStatement(queryCheck);
            st.setInt(1, groupId);
            ResultSet resultSet = st.executeQuery();
            if (resultSet.next()) {
                System.out.println("You have students in the group");
                System.out.println("Do you want to remove this group? Y/N:");
                Scanner keyboard = new Scanner(System.in);
                String answer = keyboard.nextLine();
                if ("Y".equals(answer)) {

                    statement.execute();
                    removeStudentsFromGroup(groupId);
                    System.out.println("Group removed successfully");

                } else {
                    System.out.println("Group is not removed");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}