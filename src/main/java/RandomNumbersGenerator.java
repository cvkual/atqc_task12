import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Alex on 02-May-16.
 */
public class RandomNumbersGenerator {

    private static List<Integer> studentIds = new ArrayList<>();

    public int setStudentId() {
        Random rand = new Random();
        int maxSsn = 900000;
        int minSsn = 100000;
        int id = rand.nextInt(maxSsn) + minSsn;
        boolean contain = studentIds.contains(id);
        if (contain == false) {
            studentIds.add(id);
            return id;
        } else return setStudentId();
    }

    private static List<Integer> groupIds = new ArrayList<>();

    public int setGroupId() {
        Random rand2 = new Random();
        int maxGroupId = 900;
        int minGroupId = 100;
        int groupId = rand2.nextInt(maxGroupId) + minGroupId;
        boolean contain = groupIds.contains(groupId);
        if (contain == false) {
            groupIds.add(groupId);
            return groupId;
        } else return setGroupId();
    }

    public List<Integer> getStudentIds() {
        return studentIds;
    }

    public List<Integer> getGroupIds() {
        return groupIds;
    }

    public static void main(String[] args) {
        RandomNumbersGenerator r5 = new RandomNumbersGenerator();
        RandomNumbersGenerator r8 = new RandomNumbersGenerator();
        r5.setGroupId();
        r5.setGroupId();
        r5.setGroupId();
        r5.setGroupId();
        r5.setGroupId();
        r5.setGroupId();
        r5.setGroupId();
        r5.setGroupId();
        r5.setGroupId();
        r5.setStudentId();
        r5.setStudentId();
        r5.setStudentId();
        r5.setStudentId();
        r5.setStudentId();
        r5.setStudentId();
        r5.setStudentId();
        r5.setStudentId();
        r5.setStudentId();
        r8.setStudentId();
        r8.setStudentId();
        r8.setStudentId();
        System.out.println(r5.getGroupIds());
        System.out.println(r5.getStudentIds());
        System.out.println(r8.getStudentIds());
    }
}