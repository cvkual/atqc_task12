import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

/**
 * Created by Alex on 5/16/2016.
 */
public class ParamTest1 {
    private JDBC jdbc = new JDBC();
//    private List<Student> list = jdbc.getAllStudents();
  /*  private List<Student> list2 = jdbc.getStudentsFromGroup(314);
    private List<Student> list3 = jdbc.getStudentsFromGroup(112);*/

    @DataProvider
    public Object[][] getSecondData() {
        return new Object[][] {{314, 2}, {112, 5}, {45,0}};
    }

    @Test(dataProvider = "getSecondData")
    public void testGetStudentFromGroup(int m, int n) throws Exception {
        List<Student> list2 = jdbc.getStudentsFromGroup(m);
        int count = list2.size();
        Assert.assertEquals(n, count);
    }

    public ParamTest1() throws Exception {
    }

    @DataProvider
    public Object[][] getDeletedData() {
//        return new Object[][] {{143012}}
        return new Object[][] {{143012, 7}, {7, 6}};
    }

    @Test(dataProvider = "getDeletedData")
    public void testDeleteStudent(int m1, int m2) {
        List<Student> list = jdbc.getAllStudents();
        int countbefore = list.size();
        System.out.println(countbefore);
        jdbc.deleteStudent(m1);
        int countafter = list.size();
        System.out.println(countafter);
        Assert.assertEquals(countbefore, countafter);
    }

//    @DataProvider
//    public Object[][] getData() {
//        return new Object[][] {{7, list.size()}};
//    }
//
//    @Test(dataProvider = "getData")
//    public void testGetAllStudents(int m1, int m2) throws Exception {
//        Assert.assertEquals(m1, m2);
//    }

}