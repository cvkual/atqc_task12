import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Alex on 17-May-16.
 */
public class DatabaseTesting {
    private Connection connection;
    private static Statement statement;
    private static ResultSet rs;
    private ArrayList<Student> students = new ArrayList<Student>();

    @BeforeClass
    public void setUp() {
        connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/students";
            connection = DriverManager.getConnection(url, "root", "root");
            Statement statement = null;
            ResultSet resultSet = null;
                statement = connection.createStatement();
                resultSet = statement.executeQuery(
                        "SELECT student_id, firstName, lastName, dateOfBirth, gender, group_id FROM students ORDER BY group_id");
                while (resultSet.next()) {
                    Student st = new Student(resultSet);
                    students.add(st);
                }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][] {{}};
    }

    @Test(dataProvider = "getData")
    public void testDeleteStudent(int m, int n) {

    }

    @AfterClass
    public void tearDown() {
        if (connection != null) {
            try {
                System.out.println("Closing Database Connection...");
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}
